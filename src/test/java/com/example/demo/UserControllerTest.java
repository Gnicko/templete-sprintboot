package com.example.demo;

import com.example.demo.controller.UserController;
import com.example.demo.controller.dto.UserDTO;
import com.example.demo.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    private final String RESOURCE = "/users";

    @MockBean
    private UserService userService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void allUsers() throws Exception {
        mockMvc.perform(get(RESOURCE))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void findUser() throws Exception {
        String endpoint = RESOURCE + "/" + "1";

        Mockito
                .when(userService.find(ArgumentMatchers.anyLong()))
                .thenReturn(
                        new UserDTO(
                                "joseluiscruz",
                                "jlcruz@gmail.com",
                                "http://google.com"
                        )
                );

        mockMvc.perform(get(endpoint))
                .andExpect(status().is2xxSuccessful());
    }
}
